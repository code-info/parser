package extractors

import (
	"gitlab.com/code-info/parser/internal/models"
	"gitlab.com/code-info/parser/internal/utils"
	"strings"
)

// LanguageVersion - version of language (e.g. Go 1.21)
type LanguageVersion struct {
	versionString []string
	file          string
}

func NewLanguageVersion(cfg []string, f string) *LanguageVersion {
	return &LanguageVersion{versionString: cfg, file: f}
}

func (v *LanguageVersion) Extract(content string) models.ExtractorResult {
	res := CreateResult(v.GetExtractorName())

	rows := strings.Fields(content)
	l := len(rows)
	for i, val := range rows {
		if l > i && utils.InArray(v.versionString, val) {
			res.Data = append(res.Data, models.ExtractorData{
				Value:   rows[i],   // Language name
				Version: rows[i+1], // Language version
			})
		}
	}

	return res
}

func (v *LanguageVersion) GetFileName() string {
	return v.file
}

func (v *LanguageVersion) GetExtractorName() string {
	return "LanguageVersion"
}
