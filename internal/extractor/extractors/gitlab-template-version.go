package extractors

import (
	"gitlab.com/code-info/parser/internal/models"
	"strings"
)

// GitlabTemplateVersion - version of parents gitlab template
type GitlabTemplateVersion struct {
	refString string
	file      string
}

func NewGitlabTemplateVersion(refString string, f string) *GitlabTemplateVersion {
	return &GitlabTemplateVersion{refString: refString, file: f}
}

func (r *GitlabTemplateVersion) Extract(content string) models.ExtractorResult {
	res := CreateResult(r.GetExtractorName())

	rows := strings.Fields(content)
	for i, val := range rows {
		if r.refString != val {
			continue
		}

		str := strings.ReplaceAll(rows[i+1], "'", "")
		str = strings.ReplaceAll(str, "\"", "")
		res.Data = append(res.Data, models.ExtractorData{
			Value:   rows[i],
			Version: rows[i+1],
		})
	}

	return res
}

func (r *GitlabTemplateVersion) GetFileName() string {
	return r.file
}

func (r *GitlabTemplateVersion) GetExtractorName() string {
	return "GitlabTemplateVersion"
}
