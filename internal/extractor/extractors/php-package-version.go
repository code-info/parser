package extractors

import (
	"gitlab.com/code-info/parser/internal/models"
	"strings"
)

// PhpPackageVersion - versions of php packages from composer.json
type PhpPackageVersion struct {
	packages []string
	file     string
}

func NewPhpPackage(p []string, f string) *PhpPackageVersion {
	return &PhpPackageVersion{packages: p, file: f}
}

func (p *PhpPackageVersion) Extract(content string) models.ExtractorResult {
	res := CreateResult(p.GetExtractorName())

	rows := strings.Split(content, "\n")
	var rowsRequire []string
	var rowsRequireDev []string

	// remove all json before and after "require" and "require-dev" sections
	for i, r := range rows {
		if r == "\"require\":" {
			rowsRequire = rows[i:]
		}
		if r == "\"require-dev\":" {
			rowsRequireDev = rows[i:]
		}
	}

	res.Data = p.findInRows(rowsRequire)
	res.Data = append(res.Data, p.findInRows(rowsRequireDev)...)
	return res
}

func (p *PhpPackageVersion) findInRows(rows []string) []models.ExtractorData {
	var pVersions []models.ExtractorData
	for _, row := range rows {
		if row == "}" {
			// section is over
			return pVersions
		}

		for _, p := range p.packages {
			if !strings.Contains(row, p) {
				continue
			}
			rs := strings.Split(row, " ")
			if len(rs) != 2 {
				continue
			}
			replacer := strings.NewReplacer("/v1.0/", "\"", ":", "")
			pack := replacer.Replace(rs[0])
			if pack == p {
				pVersions = append(pVersions, models.ExtractorData{
					Value:   pack,
					Version: replacer.Replace(rs[1]),
				})
			}
		}
	}

	return pVersions
}

func (p *PhpPackageVersion) GetFileName() string {
	return p.file
}

func (p *PhpPackageVersion) GetExtractorName() string {
	return "PhpPackageVersion"
}
