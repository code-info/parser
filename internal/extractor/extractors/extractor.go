package extractors

import (
	"gitlab.com/code-info/parser/internal/models"
)

const (
	LanguageVersions   = "languageVersions"
	PhpRelations       = "phpRelations"
	PhpPackageVersions = "phpPackageVersions"
	Refs               = "refVersions"
)

// Extractor - get data from content of File
type Extractor interface {
	Extract(content string) models.ExtractorResult
	// GetFileName - name of file that will use for getting data
	GetFileName() string
	GetExtractorName() string
}

func CreateResult(name string) models.ExtractorResult {
	return models.ExtractorResult{
		Name: name,
		Data: []models.ExtractorData{},
	}
}
