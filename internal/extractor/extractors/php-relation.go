package extractors

import (
	"gitlab.com/code-info/parser/internal/models"
	"strings"
)

// PhpRelation - all apps we can establish a connection from target app
type PhpRelation struct {
	file string
}

func NewPhpRelation(f string) *PhpRelation {
	return &PhpRelation{file: f}
}

func (r *PhpRelation) Extract(content string) models.ExtractorResult {
	res := CreateResult(r.GetExtractorName())
	rows := strings.Split(content, "\n")

	// remove all json before "repositories" section
	for i, r := range rows {
		if r == "\"require\":" {
			rows = rows[i:]
		}
	}

	for _, row := range rows {
		if row == "]" {
			// "repositories" section is over
			return res
		}

		i := strings.Index(row, "\"clients/")
		if i == -1 {
			continue
		}

		rs := strings.Split(row, " ")
		if len(rs) != 2 {
			continue
		}
		replacer := strings.NewReplacer("\"", ":", "")
		pack := replacer.Replace(rs[0])
		res.Data = append(res.Data, models.ExtractorData{
			Value:   pack,
			Version: replacer.Replace(rs[1]),
		})
	}

	return res
}

func (r *PhpRelation) GetFileName() string {
	return r.file
}

func (r *PhpRelation) GetExtractorName() string {
	return "PhpRelation"
}
