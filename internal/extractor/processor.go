package extractor

import (
	"gitlab.com/code-info/parser/internal/crawler"
	"gitlab.com/code-info/parser/internal/extractor/extractors"
	"gitlab.com/code-info/parser/internal/models"
	"gitlab.com/code-info/parser/internal/sender"
	"gitlab.com/code-info/parser/internal/utils"
	"log/slog"
)

// Processor - entry point of getting and handling data from git
type Processor struct {
	crawler          crawler.Crawler
	extractors       map[string]extractors.Extractor
	excludedGroups   []string // some groups may be excluded, due to the useless/old info
	excludedProjects []string // some projects may be excluded, due to the useless/old info
	sender           sender.Sender
}

func NewProcessor(
	c crawler.Crawler,
	e map[string]extractors.Extractor,
	exclGroups []string,
	exclProjects []string,
	sender sender.Sender) *Processor {
	return &Processor{
		crawler:          c,
		extractors:       e,
		excludedGroups:   exclGroups,
		excludedProjects: exclProjects,
		sender:           sender,
	}
}

func (p *Processor) Run() {
	repos := p.crawler.GetAllRepositories()

	for _, repo := range repos {
		for _, e := range p.extractors {
			go p.getRepo(e, repo)
		}
	}
}

func (p *Processor) getRepo(e extractors.Extractor, repo *models.Repo) {
	if utils.InArray(p.excludedGroups, repo.Group) || utils.InArray(p.excludedProjects, repo.Name) {
		return
	}

	file := p.crawler.GetFileFromRepository(repo.Id, e.GetFileName(), repo.DefaultBranch)
	slog.Debug("Starting extract info from file", "Extractor: ", e.GetExtractorName(), "Repo id: ", repo.Id)
	ri := models.RepoInfo{
		RepoName: repo.Name,
		Data:     e.Extract(file),
	}

	slog.Debug("Sending repository info to channel")
	p.sender.Send(ri)
}
