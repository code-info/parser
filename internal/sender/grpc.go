package sender

import (
	"gitlab.com/code-info/parser/internal/models"
	"log/slog"
)

type GrpcSender struct {
	data []models.RepoInfo
}

func NewGrpcSender() *GrpcSender {
	return &GrpcSender{data: make([]models.RepoInfo, 10)}
}

func (g *GrpcSender) Send(d models.RepoInfo) {
	slog.Debug("Receiving data from processor for sending")
	// @todo send to storage
}
