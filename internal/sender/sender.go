package sender

import "gitlab.com/code-info/parser/internal/models"

// Sender - we can send info about code different ways, e.g. sync grpc/rest/json-rpc or async kafka/*MQ/nats
type Sender interface {
	Send(d models.RepoInfo)
}
