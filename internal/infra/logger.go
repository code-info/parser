package infra

import (
	"log/slog"
	"os"
)

func InitLogger(cfg *Logger) {
	l := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: LogLevels[cfg.Level],
	}))
	slog.SetDefault(l)
}
