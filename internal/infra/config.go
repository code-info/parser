package infra

import (
	"github.com/caarlos0/env/v6"
	"gitlab.com/code-info/parser/internal/extractor/extractors"
	"log/slog"
)

type Env string

const (
	EnvDev  = "dev"
	EnvTest = "test"
	EnvProd = "prod"
)

var LogLevels = map[string]slog.Level{
	"debug": slog.LevelDebug,
	"info":  slog.LevelInfo,
	"warn":  slog.LevelWarn,
	"error": slog.LevelError,
}

type (
	Config struct {
		Logger Logger
		App    App
	}

	Logger struct {
		Level string `env:"LOG_LEVEL" envDefault:"debug"`
	}

	Crawler struct {
		GitToken  string `env:"GIT_TOKEN"`
		GitDomain string `env:"GIT_DOMAIN"`
	}

	Extractors struct {
		Enabled []string `env:"EXTRACTORS" envDefault:"language,version,phpRelations,refVersions"`
		Version struct {
			Versions []string `env:"EXTRACTORS_LANGUAGES_VERSIONS" envDefault:"PHP_VERSION:,GO_VERSION:,image:" envSeparator:","`
			File     string   `env:"EXTRACTORS_LANGUAGES_FILE" envDefault:".gitlab-ci.yml"`
		}
		PhpPackageVersion struct {
			Versions []string `env:"EXTRACTORS_PHP_PACKAGE_VERSIONS" envDefault:"symfony/framework-bundle" envSeparator:","`
			File     string   `env:"EXTRACTORS_PHP_PACKAGE_VERSIONS_FILE" envDefault:"composer.json"`
		}
		PhpRelation struct {
			File string `env:"EXTRACTORS_PHP_RELATIONS_FILE" envDefault:"composer.json"`
		}
		RefVersion struct {
			RefString string `env:"EXTRACTORS_REF_VERSION" envDefault:"ref:"`
			File      string `env:"EXTRACTORS_REF_FILE" envDefault:".gitlab-ci.yml"`
		}
	}

	App struct {
		Crawler         Crawler    `env:"crawler"`
		Extractors      Extractors `env:"extractors"`
		ExcludeGroups   []string   `env:"EXCLUDE_GROUPS" envDefault:"docker,infrastructure" envSeparator:","`
		ExcludeProjects []string   `env:"EXCLUDE_PROJECTS" envDefault:""`
		Env             Env        `env:"APP_ENV" envDefault:"dev"`
		MaxThreads      int        `env:"APP_MAX_THREADS" envDefault:"2"`
	}
)

func GetConfig() Config {
	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		slog.Error("get config error. ", err)
	}

	return cfg
}

func GetExtractors(cfg *Config) map[string]extractors.Extractor {
	ext := make(map[string]extractors.Extractor)
	extrs := cfg.App.Extractors
	for _, v := range extrs.Enabled {
		if v == extractors.LanguageVersions {
			ext[v] = extractors.NewLanguageVersion(extrs.Version.Versions, extrs.Version.File)
		}

		if v == extractors.PhpPackageVersions {
			ext[v] = extractors.NewPhpPackage(extrs.PhpPackageVersion.Versions, extrs.PhpPackageVersion.File)
		}

		if v == extractors.PhpRelations {
			ext[v] = extractors.NewPhpRelation(extrs.PhpRelation.File)
		}

		if v == extractors.Refs {
			ext[v] = extractors.NewGitlabTemplateVersion(extrs.RefVersion.RefString, extrs.RefVersion.File)
		}
	}

	return ext
}
