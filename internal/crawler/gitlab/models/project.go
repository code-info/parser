package models

import "time"

// ShortProject - actually, I don't need full project struct, so I use ShortProject for values I need only
type ShortProject struct {
	Id            int       `json:"id"`
	Name          string    `json:"name"`
	WebUrl        string    `json:"web_url"`
	DefaultBranch string    `json:"default_branch"`
	Namespace     Namespace `json:"namespace"`
}

type Namespace struct {
	Id        int    `json:"id"`
	Name      string `json:"name"`
	Path      string `json:"path"`
	Kind      string `json:"kind"`
	FullPath  string `json:"full_path"`
	ParentId  *int   `json:"parent_id"`
	AvatarUrl string `json:"avatar_url"`
	WebUrl    string `json:"web_url"`
}

type Links struct {
	Self          string `json:"self"`
	Issues        string `json:"issues"`
	MergeRequests string `json:"merge_requests"`
	RepoBranches  string `json:"repo_branches"`
	Labels        string `json:"labels"`
	Events        string `json:"events"`
	Members       string `json:"members"`
	ClusterAgents string `json:"cluster_agents"`
}

type ContainerExpirationPolicy struct {
	Cadence       string  `json:"cadence"`
	Enabled       bool    `json:"enabled"`
	KeepN         int     `json:"keep_n"`
	OlderThan     string  `json:"older_than"`
	NameRegex     string  `json:"name_regex"`
	NameRegexKeep *string `json:"name_regex_keep"`
	NextRunAt     string  `json:"next_run_at"`
}

type GroupAccess struct {
	AccessLevel       int `json:"access_level"`
	NotificationLevel int `json:"notification_level"`
}

type Permissions struct {
	ProjectAccess any         `json:"project_access"`
	GroupAccess   GroupAccess `json:"group_access"`
}

// Project - full info about project by Gitlab API. Project = repository in Gitlab`s context
type Project struct {
	Id                                        int                        `json:"id"`
	Name                                      string                     `json:"name"`
	Description                               string                     `json:"description"`
	NameWithNamespace                         string                     `json:"name_with_namespace"`
	Path                                      string                     `json:"path"`
	PathWithNamespace                         string                     `json:"path_with_namespace"`
	CreatedAt                                 string                     `json:"created_at"`
	DefaultBranch                             string                     `json:"default_branch"`
	TagList                                   []string                   `json:"tag_list"`
	Topics                                    []string                   `json:"topics"`
	SshUrlToRepo                              string                     `json:"ssh_url_to_repo"`
	HttpUrlToRepo                             string                     `json:"http_url_to_repo"`
	WebUrl                                    string                     `json:"web_url"`
	ReadmeUrl                                 *string                    `json:"readme_url"`
	ForksCount                                int                        `json:"forks_count"`
	AvatarUrl                                 *string                    `json:"avatar_url"`
	StarCount                                 int                        `json:"star_count"`
	LastActivityAt                            string                     `json:"last_activity_at"`
	Namespace                                 *Namespace                 `json:"namespace"`
	ContainerRegistryImagePrefix              string                     `json:"container_registry_image_prefix"`
	PackagesEnabled                           bool                       `json:"packages_enabled"`
	EmptyRepo                                 bool                       `json:"empty_repo"`
	Archived                                  bool                       `json:"archived"`
	Visibility                                string                     `json:"visibility"`
	ResolveOutdatedDiffDiscussions            bool                       `json:"resolve_outdated_diff_discussions"`
	Links                                     *Links                     `json:"_links"`
	ContainerExpirationPolicy                 *ContainerExpirationPolicy `json:"container_expiration_policy"`
	IssuesEnabled                             bool                       `json:"issues_enabled"`
	MergeRequestsEnabled                      bool                       `json:"merge_requests_enabled"`
	WikiEnabled                               bool                       `json:"wiki_enabled"`
	JobsEnabled                               bool                       `json:"jobs_enabled"`
	SnippetsEnabled                           bool                       `json:"snippets_enabled"`
	ContainerRegistryEnabled                  bool                       `json:"container_registry_enabled"`
	ServiceDeskEnabled                        bool                       `json:"service_desk_enabled"`
	ServiceDeskAddress                        *string                    `json:"service_desk_address"`
	CanCreateMergeRequestIn                   bool                       `json:"can_create_merge_request_in"`
	IssuesAccessLevel                         string                     `json:"issues_access_level"`
	RepositoryAccessLevel                     string                     `json:"repository_access_level"`
	MergeRequestsAccessLevel                  string                     `json:"merge_requests_access_level"`
	ForkingAccessLevel                        string                     `json:"forking_access_level"`
	WikiAccessLevel                           string                     `json:"wiki_access_level"`
	BuildsAccessLevel                         string                     `json:"builds_access_level"`
	SnippetsAccessLevel                       string                     `json:"snippets_access_level"`
	PagesAccessLevel                          string                     `json:"pages_access_level"`
	AnalyticsAccessLevel                      string                     `json:"analytics_access_level"`
	ContainerRegistryAccessLevel              string                     `json:"container_registry_access_level"`
	SecurityAndComplianceAccessLevel          string                     `json:"security_and_compliance_access_level"`
	ReleasesAccessLevel                       string                     `json:"releases_access_level"`
	EnvironmentsAccessLevel                   string                     `json:"environments_access_level"`
	FeatureFlagsAccessLevel                   string                     `json:"feature_flags_access_level"`
	InfrastructureAccessLevel                 string                     `json:"infrastructure_access_level"`
	MonitorAccessLevel                        string                     `json:"monitor_access_level"`
	EmailsDisabled                            *bool                      `json:"emails_disabled"`
	SharedRunnersEnabled                      bool                       `json:"shared_runners_enabled"`
	LfsEnabled                                bool                       `json:"lfs_enabled"`
	CreatorId                                 int                        `json:"creator_id"`
	ImportUrl                                 *string                    `json:"import_url"`
	ImportType                                *string                    `json:"import_type"`
	ImportStatus                              string                     `json:"import_status"`
	OpenIssuesCount                           int                        `json:"open_issues_count"`
	DescriptionHtml                           string                     `json:"description_html"`
	UpdatedAt                                 *time.Time                 `json:"updated_at"`
	CiDefaultGitDepth                         int                        `json:"ci_default_git_depth"`
	CiForwardDeploymentEnabled                bool                       `json:"ci_forward_deployment_enabled"`
	CiJobTokenScopeEnabled                    bool                       `json:"ci_job_token_scope_enabled"`
	CiSeparatedCaches                         bool                       `json:"ci_separated_caches"`
	CiAllowForkPipelinesToRunInParentProject  bool                       `json:"ci_allow_fork_pipelines_to_run_in_parent_project"`
	BuildGitStrategy                          string                     `json:"build_git_strategy"`
	KeepLatestArtifact                        bool                       `json:"keep_latest_artifact"`
	RestrictUserDefinedVariables              bool                       `json:"restrict_user_defined_variables"`
	RunnersToken                              string                     `json:"runners_token"`
	RunnerTokenExpirationInterval             *string                    `json:"runner_token_expiration_interval"`
	GroupRunnersEnabled                       bool                       `json:"group_runners_enabled"`
	AutoCancelPendingPipelines                string                     `json:"auto_cancel_pending_pipelines"`
	BuildTimeout                              int                        `json:"build_timeout"`
	AutoDevopsEnabled                         bool                       `json:"auto_devops_enabled"`
	AutoDevopsDeployStrategy                  string                     `json:"auto_devops_deploy_strategy"`
	CiConfigPath                              *string                    `json:"ci_config_path"`
	PublicJobs                                bool                       `json:"public_jobs"`
	SharedWithGroups                          []string                   `json:"shared_with_groups"`
	OnlyAllowMergeIfPipelineSucceeds          bool                       `json:"only_allow_merge_if_pipeline_succeeds"`
	AllowMergeOnSkippedPipeline               any                        `json:"allow_merge_on_skipped_pipeline"`
	RequestAccessEnabled                      bool                       `json:"request_access_enabled"`
	OnlyAllowMergeIfAllDiscussionsAreResolved bool                       `json:"only_allow_merge_if_all_discussions_are_resolved"`
	RemoveSourceBranchAfterMerge              bool                       `json:"remove_source_branch_after_merge"`
	PrintingMergeRequestLinkEnabled           bool                       `json:"printing_merge_request_link_enabled"`
	MergeMethod                               string                     `json:"merge_method"`
	SquashOption                              string                     `json:"squash_option"`
	EnforceAuthChecksOnUploads                bool                       `json:"enforce_auth_checks_on_uploads"`
	SuggestionCommitMessage                   *string                    `json:"suggestion_commit_message"`
	MergeCommitTemplate                       *string                    `json:"merge_commit_template"`
	SquashCommitTemplate                      *string                    `json:"squash_commit_template"`
	IssueBranchTemplate                       *string                    `json:"issue_branch_template"`
	AutocloseReferencedIssues                 bool                       `json:"autoclose_referenced_issues"`
	RepositoryStorage                         string                     `json:"repository_storage"`
	Permissions                               Permissions                `json:"permissions"`
}
