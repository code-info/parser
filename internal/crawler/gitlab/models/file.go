package models

// File - info about file from Project (includes content of file)
type File struct {
	FileName        string `json:"file_name"`
	FilePath        string `json:"file_path"`
	Size            int    `json:"size"`
	Encoding        string `json:"encoding"`
	Content         string `json:"content"`
	ContentSha256   string `json:"content_sha256"`
	Ref             string `json:"ref"`
	BlobId          string `json:"blob_id"`
	CommitId        string `json:"commit_id"`
	LastCommitId    string `json:"last_commit_id"`
	ExecuteFileMode bool   `json:"execute_filemode"`
}
