package gitlab

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/code-info/parser/internal/crawler/gitlab/models"
	commonModels "gitlab.com/code-info/parser/internal/models"
	"io"
	"log/slog"
	"net/http"
)

const projects = "/projects?page=%d&per_page=100"
const file = "/projects/%d/repository/files/%s?ref=%s"

// Crawler - collect and parse data from Gitlab
type Crawler struct {
	token   string
	domain  string
	cache   map[int]map[string]string
	page    int
	mutator *Mutator
}

func NewCrawler(token string, domain string) *Crawler {
	return &Crawler{
		token:   token,
		domain:  domain,
		cache:   make(map[int]map[string]string),
		page:    1,
		mutator: NewMutator(),
	}
}

func (c *Crawler) GetAllRepositories() (repos []*commonModels.Repo) {
	for {
		url := c.domain + fmt.Sprintf(projects, c.page)
		slog.Debug("Get projects info", "Page: ", c.page, "Url: ", url)
		jsonRes := c.sendRequest(url)
		c.page++

		var r []*models.ShortProject
		if err := json.Unmarshal(jsonRes, &r); err != nil {
			slog.Error("Error during unmarshalling projects. ", "Page: ", c.page, "Err", err)
		}
		slog.Debug("Info about project was unmarshalled. ", "Page: ", c.page)
		if len(r) == 0 {
			slog.Debug("All projects was handled")
			return
		}
		for _, v := range r {
			repos = append(repos, c.mutator.mutate(v))
		}
		slog.Debug("Successfully mutate all projects. ", "Page: ", c.page)
	}
}

func (c *Crawler) GetFileFromRepository(id int, name string, branch string) string {
	f, ok := c.cache[id][name]
	if ok {
		return f
	}

	url := c.domain + fmt.Sprintf(file, id, name, branch)
	slog.Debug("Get file", "Url: ", url, "Repo id: ", id)
	jsonRes := c.sendRequest(url)

	var file models.File
	if err := json.Unmarshal(jsonRes, &file); err != nil {
		slog.Error("Error during unmarshalling file from project.", "Url: ", url, "Project id", id)
	}
	slog.Debug("File from project was unmarshalled")

	str, err := base64.StdEncoding.DecodeString(file.Content)
	if err != nil {
		slog.Error("Error during base64 decoding file from project.", "Url: ", url, "Repo id", id)
	}
	slog.Debug("File from project was decoded")

	content := string(str)
	c.cache[id] = map[string]string{name: content}

	return content
}

func (c *Crawler) sendRequest(url string) []byte {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		slog.Error("error during creating request for getting projects. ", "Err", err)
	}
	req.Header.Add("Authorization", "Bearer "+c.token)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		slog.Error("error during getting projects. ", "Err", err)
	}

	res, err := io.ReadAll(resp.Body)
	if err != nil {
		slog.Error("error during getting body from projects response. ", "Err", err)
	}
	if err = resp.Body.Close(); err != nil {
		slog.Error("error during closing response body. ", "Err", err)
	}

	return res
}
