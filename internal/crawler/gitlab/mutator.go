package gitlab

import (
	models2 "gitlab.com/code-info/parser/internal/crawler/gitlab/models"
	"gitlab.com/code-info/parser/internal/models"
)

// Mutator - cast data from specific gitlab format to more extractor models.Repo
type Mutator struct{}

func NewMutator() *Mutator {
	return &Mutator{}
}

func (m *Mutator) mutate(p *models2.ShortProject) *models.Repo {
	r := models.Repo{
		Id:            p.Id,
		Name:          p.Name,
		DefaultBranch: p.DefaultBranch,
		Url:           p.WebUrl,
	}

	if p.Namespace.Kind == "group" {
		r.Group = p.Namespace.Name
	}

	return &r
}
