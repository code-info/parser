package crawler

import "gitlab.com/code-info/parser/internal/models"

// Crawler - collect and parse data from repository
type Crawler interface {
	GetAllRepositories() (repos []*models.Repo)
	GetFileFromRepository(id int, name string, branch string) string
}
