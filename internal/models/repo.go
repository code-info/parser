package models

// Repo - normalized struct with info about repository
type Repo struct {
	Id            int
	Name          string
	Url           string
	DefaultBranch string
	Group         string // name of group of repository if supported (GitHub doesn't support)
	Kind          string // Gitlab/GitHub/Bitbucket, etc
}
