package models

type ExtractorResult struct {
	Name string
	Data []ExtractorData
}

type ExtractorData struct {
	Value   string
	Version string
}
