package models

// RepoInfo - structured info about repository
type RepoInfo struct {
	RepoName string
	Data     ExtractorResult
}
