package main

import (
	"gitlab.com/code-info/parser/internal/crawler/gitlab"
	"gitlab.com/code-info/parser/internal/extractor"
	"gitlab.com/code-info/parser/internal/infra"
	"gitlab.com/code-info/parser/internal/sender"
	"log"
	"runtime"
)

func main() {
	cfg := infra.GetConfig()
	runtime.GOMAXPROCS(cfg.App.MaxThreads)
	infra.InitLogger(&cfg.Logger)

	extractor.NewProcessor(
		gitlab.NewCrawler(cfg.App.Crawler.GitToken, cfg.App.Crawler.GitDomain),
		infra.GetExtractors(&cfg),
		cfg.App.ExcludeGroups,
		cfg.App.ExcludeProjects,
		sender.NewGrpcSender(),
	).Run()

	log.Println("Has done")
}
